
import animation.*;
import imt2018033.*;
//import demo.*;

// Driver class to set up and exercise the animation
public class Main {

	public static void main(String[] args) {
		Scene scene = new SceneExtended(); /*{  // replace with appropriate derived class
			protected void checkScene() {}
		};*/
		
		//scene.setView(new TextView()); // can be replaced with other Views that will be provided
		
		// populate the scene with objects implemented by the team
		
		for(int i=0;i<6;i++) {
			SceneObject s = new SceneObjectExtended();
			s.setPosition(i*50, i*50);  
			scene.addObstacle(s); // using appropriate derived classes
		}
		
			
		for(int i=0;i<6;i++) {
			SceneObject s = new SceneObjectExtended();
			s.setPosition(500 - i*50, 300 + i*50);  // these will be changed for the demo
			s.setDestPosition(500, 0);
			scene.addActor(s); // using appropriate derived classes
		}

		View view = new DemoSwingView();

		scene.setView(view);

		view.init();
				
		//scene.animate();
	}

}
