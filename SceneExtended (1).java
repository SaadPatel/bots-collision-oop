package imt2018033;

import animation.*;

import java.util.ArrayList;

public class SceneExtended extends Scene
{
    protected void checkScene()
    {
        ArrayList <SceneObject> actors = getActors();
        ArrayList <SceneObject> obstacles = getObstacles();

        
        for(int i = 0; i < actors.size(); i++)
        {
            for(int j = i + 1; j < actors.size(); j++)
            {

                
                if(actors.get(i).getBBox().intersects(actors.get(j).getBBox()))
                {
                    System.out.println("Collision between " + actors.get(i).getObjName() +" and " + actors.get(j).getObjName());
                    //actors.remove(i);
                    //i--;
                    //j--;
                    //actors.remove(j);
                    //j--;
                    break;
                }
            }    

            for(int j = 0; j < obstacles.size(); j++)
            {   
                //System.out.println( actors.get(i).getBBox().getMinPt() +"   " + obstacles.get(j).getBBox().getMinPt() );
                
                if(actors.get(i).getBBox().intersects(obstacles.get(j).getBBox()))
                {
                    System.out.println("Collision between " + actors.get(i).getObjName() +" and " + obstacles.get(j).getObjName());
                    //actors.remove(i);
                    //j--;
                    break;
                }    
            }
        }
    }

    
}