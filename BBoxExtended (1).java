package imt2018033;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Collections;
import animation.*;

public class BBoxExtended implements BBox
{
    private Point min;
    private Point max;
    private int sizex;
    private int sizey;

    public BBoxExtended(ArrayList<Point> outline)
    {
        ArrayList<Point> temp = outline;
        Comparator<Point> compareByX = (Point a, Point b) -> (a.getX() > b.getX())? 1: -1;
        Comparator<Point> compareByY = (Point a, Point b) -> (a.getY() > b.getY())? 1: -1;

        Collections.sort(temp, compareByX);
        int minx = temp.get(0).getX();
        int maxx = temp.get(temp.size() - 1).getX();
        
        Collections.sort(temp, compareByY);
        int miny = temp.get(0).getY();
        int maxy = temp.get(temp.size() - 1).getY();

        min = new Point(minx, miny);
        max = new Point(maxx, maxy);
        sizex = maxx - minx;
        sizey = maxy - miny;
    }

    public BBoxExtended(BBox a)
    {
        min = new Point(a.getMinPt());
        max = new Point(a.getMaxPt());
    }

    public Point getMinPt()
    {
        return min;
    }

    public Point getMaxPt()
    {
        return max;
    }

    public boolean intersects(BBox b)
    {
       /* if(min.getX() > b.getMaxPt().getX() || max.getX() < b.getMinPt().getX() )
                return false;
        if(min.getY() > b.getMaxPt().getY() || max.getX() < b.getMinPt().getY() )
            return false;
        //System.out.println(min.getX() + " " + b.getMaxPt().getX() + " " + max.getX() + " " + b.getMinPt().getX() );    
        return true;    
        */
        if(min.getX()  > b.getMaxPt().getX() + 20|| max.getX()  < b.getMinPt().getX() - 20 )
            return false;
        if(min.getY()  > b.getMaxPt().getY() + 20 || max.getY()  < b.getMinPt().getY() - 20 )
            return false;
//System.out.println(min.getX() + " " + b.getMaxPt().getX() + " " + max.getX() + " " + b.getMinPt().getX() );    
        return true;
            
    }

    public String toString()
    {
        return max + " " + min;
    }

    public void makeBuffer()
    {
        min.setPos(min.getX() - 20, min.getY() - 20);
        max.setPos(max.getX() + 20, max.getY() + 20);
    }

    public boolean bufferIntersects(BBox b)
    {
        if(min.getX() - 20 > b.getMaxPt().getX() + 20|| max.getX() + 20 < b.getMinPt().getX() - 20 )
            return false;
        if(min.getY() - 20 > b.getMaxPt().getY() + 20 || max.getX() + 20 < b.getMinPt().getY() - 20 )
            return false;
//System.out.println(min.getX() + " " + b.getMaxPt().getX() + " " + max.getX() + " " + b.getMinPt().getX() );    
        return true; 
    }

    public static void main(String args[])
    {
        ArrayList<Point> a = new ArrayList<Point>();
        a.add(new Point(200,200));
        a.add(new Point(201,201));
        ArrayList<Point> z = new ArrayList<Point>();
        z.add(new Point(200,160));
        z.add(new Point(201,261));
        
        BBoxExtended b = new BBoxExtended(a);
        BBoxExtended c = new BBoxExtended(z);
        System.out.println(b.intersects(c));
    }
}