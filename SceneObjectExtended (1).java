package imt2018033;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


import animation.*;

public class SceneObjectExtended extends SceneObject
{
    private String name;
    private Point position;
    private static Integer number = 1; 
    private int dx;
    private int dy;
    private Point destination;
    private ArrayList<Point> outline;
    private BBoxExtended bBox;
    private BBoxExtended buffer;

    public SceneObjectExtended()
    {
        name = number.toString();
        position = new Point(0,0);
        destination = new Point(0,0);
        outline = new ArrayList<Point>();
        outline.add(new Point(0,0));
        outline.add(new Point(20,20));
        bBox = new BBoxExtended(outline);
        buffer = new BBoxExtended(outline);
        buffer.makeBuffer();
        number++;
    
    }

    @Override
    public String getObjName()
    {
        return name;
    }

    @Override
    public Point getPosition()
    {
        return position;
    }

    @Override
    public void setPosition(int x, int y)
    {
        Point oldPos = new Point(position);
        position.setPos(x, y);
        int dx = position.getX() - oldPos.getX();
        int dy = position.getY() - oldPos.getY();

        

        for(Point p: outline)
        {
            p.setPos(p.getX() + dx, p.getY() + dy);
        }

        bBox = new BBoxExtended(outline);
        buffer = new BBoxExtended(outline);
        buffer.makeBuffer();

        System.out.println("bbox " + bBox);
        
    }

    public void setDestPosition(int x, int y)
    {
        destination.setPos(x, y);
    }

    @Override
    public BBox getBBox()
    {
        return bBox;
    }

    public BBox getBuffer()
    {
        return buffer;
    }

    @Override
    public ArrayList<Point> getOutline()
    {
        return outline;
    }

    public boolean checkPos(int dx, int dy)
    {
        ArrayList<Point> tempOutline = new ArrayList<Point>();


        for(Point p : outline)
        {
            tempOutline.add(new Point(p));
        }

        for(Point p: tempOutline)
        {
            p.setPos(p.getX() + dx, p.getY() + dy);
        }
        

        BBoxExtended tempBBox = new BBoxExtended(tempOutline);
        //tempBBox.makeBuffer();


        ArrayList <SceneObject> actors = Scene.getScene().getActors();
        ArrayList <SceneObject> obstacles = Scene.getScene().getObstacles();



        for(int i = 0; i < actors.size(); i++)
        {   
            
            if(actors.get(i).getBBox().intersects(tempBBox) && actors.get(i) != this)
            {
                System.out.println("change direction");
                return true;
            }
        } 

        for(int i = 0; i < obstacles.size(); i++)
        {
            if(obstacles.get(i).getBBox().intersects(tempBBox))
            {   
                System.out.println("obs " + obstacles.get(i).getBBox());
                System.out.println("temp " + tempBBox);
                return true;
            }
        } 


        return false;
    }

    @Override
    protected void updatePos(int deltaT)
    {
        int tempdx = destination.getX() - position.getX();
        int tempdy = destination.getY() - position.getY();

        if(tempdx == 0 && tempdy == 0)
        {
            System.out.println("Object " + name + " has reached its destination");
            Scene.getScene().getActors().remove(this);
        }

        if(Math.abs(tempdy) > 20)
        {
            tempdy = 20 * (tempdy / Math.abs(tempdy));
        }

        if(Math.abs(tempdx) > 20)
        {
            tempdx = 20 * (tempdx / Math.abs(tempdx));
        }

//        ArrayList<Point> tempOutline = new ArrayList<Point>();
//        BBoxExtended temp = new BBoxExtended();

        //setPosition(position.getX() + dx, position.getY() + dy);
        //System.out.println(name + bBox.getMinPt());

        ArrayList <SceneObject> actors = Scene.getScene().getActors();
        ArrayList <SceneObject> obstacles = Scene.getScene().getObstacles();

        dx = tempdx;
        dy = tempdy;

        if(checkPos(dx, dy))
        {
            dx = - tempdy;
            dy = tempdx;
            
        }
        if(checkPos(dx, dy))
        {
            dx =  tempdy;
            dy = - tempdx;
        }
        if(checkPos(dx, dy))
        {
            dx = - tempdx;
            dy = - tempdy;
        }
        if(checkPos(dx, dy))
        {
            dx = 0;
            dy = 0;
        }

        setPosition(position.getX() + dx, position.getY() + dy);

    }    

        

        

}

